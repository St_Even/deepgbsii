#!/bin/bash

# $1 parameters.txt
# $2 jobList.txt

#./iterator.sh parameters.txt jobList.txt

#for each line in joblist
while read fc
	do
	#test line is not completed
	if [ "${fc}" != "COMPLETED" ]
		then

		# replace the FLOWCELL=* line in parameters
                	find="FLOWCELL="
			str="FLOWCELL=${fc}"
			echo "Iterator calling deepGBSII with: ${str}"
	                #find line and replace
			sed -i "s:${find}.*:${str}:g" $1
		# replace the *LANES=* line in parameters
			find="LANES="
                        str="${fc}_LANES=1 2"
			sed -i "s:.*${find}.*:${str}:g" $1
        	# call deepGBSII
	        ./deepGBSII.sh $1
        	#test exit status
		if [ $? -ne 0 ]
			then
	        	#error status
			printf "There is a problem with the job list line: ${fc}\n"
			exit 1
		else
			#clear status, remove the line from the joblist
        	        find="${fc}"
			str="COMPLETED"
                	#find job line and replace with COMPLETED
			sed -i "s:${find}:${str}:" $2
		fi
	fi

done < $2

echo "The iterator has completed the job list."
exit
