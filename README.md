# deepGBSII for Paired Data and Large Plant Genomes  
Steven Holden  
steven.douglas.holden@gmail.com  

built on jerlar73/fast-gbs  
Jerome Laroche  

A bioinformatic pipeline designed to extract a high-quality SNP catalog starting from paired end FASTQ read files obtained from genotyping-by-sequencing (GBS) libraries.

deepGBSII for paired data follows the proven fastGBS pipeline and adds  
	- Processing large plant genomes  
	- Paired end read processing  
	- Joblist for batch processing  
	- SNP filtering and imputation included in the pipeline  
	- Improved logfile outputs and handling  
	- Export functions to specified location  
	- Intermediate .sort.bam files may be exported separately  
	- Completed results and logfiles packaged and exported  
	- Data is removed when it is no longer required,  
	- Tests check for completeness of processing  
	- DataLib can contain symbolic links pointing off of the processing computer to save harddrive space  
	- The most storage heavy steps are designed to use minimal storage  
	- Processsing heavy steps are parallel processed to use 80% of available cores  


## Introduction

deepGBSII for paired data is a bash pipeline facilitating the processing of paired FASTQ sequence files obtained using a genotyping-by-sequencing method (GBS).
The design of the pipeline departs from fast-gbs where large plant genomes exceed the indexing capacity of the previous design.
The design and quality of results from this pipeline is supported by  

	Yao, Z., You, F.M., N Diaye, A. et al. Evaluation of variant calling tools for large plant genome re-sequencing. BMC Bioinformatics 21, 360 (2020). https://doi.org/10.1186/s12859-020-03704-1

It includes a set of bash commands, python scripts from fast-gbs, and well-known bioinformatics software such as sabre, bwa, samtools etc. 
Users fill out a parameter file and then launch the program. 
For each analysis, it requires the existence of a reference genome. 
This may be a relatively complete genome made of pseudochromosomes or a more preliminary draft consisting of scaffolds and contigs.

Go to the Wiki page to see all the documentation on Fast-GBS
https://bitbucket.org/jerlar73/fast-gbs/wiki/Home

## Citation 

If you use this pipeline in published work cite the following components:   

###### FastGBS  
Torkamaneh, D., Laroche, J., Bastien, M., Abed, A., & Belzile, F. (2017). Fast-GBS: a new pipeline for the efficient and highly accurate calling of SNPs from genotyping-by-sequencing data. BMC bioinformatics, 18(1), 5. https://doi:10.1186/s12859-016-1431-9  
###### Parallel  
O. Tange (2011): GNU Parallel - The Command-Line Power Tool, ;login: The USENIX Magazine, February 2011:42-47.  
###### Cutadapt  
DOI: https://doi.org/10.14806/ej.17.1.200  
###### BWA  
Li H. and Durbin R. (2010) Fast and accurate long-read alignment with Burrows-Wheeler Transform. Bioinformatics, Epub. [PMID: 20080505]  
###### Samtools/Bcftools  
Li H, Handsaker B, Wysoker A, Fennell T, Ruan J, Homer N, Marth G, Abecasis G, Durbin R; 1000 Genome Project Data Processing Subgroup. The Sequence Alignment/Map format and SAMtools. Bioinformatics. 2009 Aug 15;25(16):2078-9. doi: 10.1093/bioinformatics/btp352. Epub 2009 Jun 8. PMID: 19505943; PMCID: PMC2723002.  
###### Beagle 5.1  
S R Browning and B L Browning (2007). Rapid and accurate haplotype phasing and missing data inference for whole genome association studies by use of localized haplotype clustering. Am J Hum Genet 81:1084-97. doi:10.1086/521987  




## Glossary

- FLOWCELL: Sequencing batch. Originally can contain up to 8 lanes  
- LANES: File containing multiplexed sequences. For paired data these will be reads 1 and 2, where 1 is the forward read and 2 is the reverse read.  

## Using deepGBSII

The main steps in using deepGBSII are:  
Go to or create the directory in which you wish to perform the analysis and store results  
Clone the repository (detailed below)
Set up the environment (detailed below)  
Run the script `./makeDir.sh` that will create the following directories:  

	refgenome  
	data  
	dataLib  
	barcodes  
	results  
	resultsStored

Prepare the reference genome (detailed below) and place it in the refgenome directory  
Prepare the barcode files (detailed below) and place them in the barcodes directory  
Prepare the data (detailed below)  
Run the linker script from the main directory. The script contains details on its use and purpose.  
Prepare the parameters file (detailed below) and place it in the same directory in which you launched the ./makeDir.sh script  
Activate the conda environment  
Run the script deepGBSII.sh  

## Dependencies

In order to use deepGBSII, you will need the following:  

### Linux OS

### Pull this distribution from bitbucket
cd to the install location and run
	
	git clone https://bitbucket.org/St_Even/deepgbsii.git

this will install

	- deepGBSII.sh (this distribution)  
	- parameters.txt (this distribution)  
	- makeDir.sh (this distribution)  
	- makeBarcodeSabre.py (from fast-GBS)  
	- vcf2txt.py (from fast-GBS)  
	- txt2unix.sh (from fast-GBS)  
	- linker.sh (this distribution)  
	- iterator.sh (this distribution)  
	- environment_deepGBSII.yml (this distribution)  

### Setup the environment  
With conda [installed](https://docs.anaconda.com/anaconda/install/linux/)
cd into the installed directory and run

	conda env create -f environment_deepGBSII.yml

Creating a bioconda environment containing  

	- Gnu parallel  
	- Python 2.7 version exact  
	- sabre  
	- cutadapt  
	- bwa  
	- samtools  
	- beagle  
	- cutadapt  
	- fastqc  
	- multiqc  
	- bcftools  
	- vcftools  
	- matplotlib  
	- vcf python module (https://github.com/jamescasbon/PyVCF)  
	- etc...  

A visualized QC option is built and commented out, currently untested, it depends on a LaTeX distribution being installed, which can't be pulled from conda.  
## Quality control

The [fastqc](http://www.bioinformatics.babraham.ac.uk/projects/fastqc/) software is not included in the pipeline but it is strongly recommended to perform a quality check on your sequence files with it before starting deepGBSII. 
Running multiQC on the fastQC results is highly recomended. A (partial) script to do these steps has been included.

## Preparation of the reference genome

Move to your reference genome file in the refgenome directory, decompress and index it with the commands:  
	
	bgzip -d refgenome.fa.gz
	bwa index -a bwtsw refgenome.fa
	
The .fai file isn't created as part of "bwa index." To create it, run the commands:  
	
	samtools faidx refgenome.fa  
	
That will create a file named refgenome.fasta.fai.  
Finally, write the reference genome file name in the parameter file:  
	
	REFGEN=refgenome.fasta  
	

## Preparation of the data  

Move your to sequence files in their directory, before running the linker script. Each file must be named in the form:  
	
	<FLOWCELL>_<READ>.fq.gz  
	
Suppose that you want to analyse 6 sequence files over 3 flowcells (Odin, Thor and Freya) and each flowcell has 2 reads:  

	Odin: reads 1 and 2  
	Thor: reads 1 and 2  
	Freya: reads 1 and 2
	
Name the files the following way:  

	Odin_1.fq.gz  
	Odin_2.fq.gz  
	Thor_1.fq.gz  
	Thor_2.fq.gz  
	Freya_1.fq.gz  
	Freya_2.fq.gz  

It is important to keep the extension .fq.gz because, for simplicity, it is hardcoded in the pipeline.  
Once the files are named, the linker script can be run to create links in dataLib

	./linker "/path/to/DirContainingData"

The symbolic links take up little room and can point to network drives as long as they remain mounted. 
This saves space and offers a layer of protection for your data. 
Archived copies of data are recommended, backup regularly, do not learn this the hard way.

Finally, write the flowcell name and lane names( lanes are used to specify FWD REV reads ) at these places in the parameters file:  

	; FLOWCELL INFORMATION  
	FLOWCELL=Odin  
	; LANES INFORMATION  
	Odin_LANES=1 2

In a subsequent single run modify the parameters file to process a separate flowcell, this is done automatically by the iterator script  

	; FLOWCELL INFORMATION  
	FLOWCELL=Freya  
	; LANES INFORMATION  
	Freya_LANES=1 2

## Preparation of the barcode files

You can use any spreadsheet software (openOffice, Excel) to format your barcodes files.   
If you do so, save the active sheet in text (.txt) format with tab separated columns.  
Then move your files to your Unix computer. It is possible that formatting (the carriage return) is not recognized by Unix system.   
To convert your file in ASCII, use the script txt2unix.sh available from the fast-gbs distribution:  
	
	./txt2unix.sh your_barcodes_file > new_barcodes_file
	
Continuing with the same example, you will have to create one barcode file per sequence file and name them as below:  
	
	barcodes_Odin_1  
	barcodes_Odin_2  
	barcodes_Thor_1  
	barcodes_Thor_2  
	barcodes_Freya_1  
	barcodes_Freya_2

Do not add any extension, like .txt , to the filenames.  
the content of these files follows the format: barcode tab sampleName  

	GTATTA	samplename     
	CTATTA	synergy 
	TAACGA	AC-Metcalfe 
	CGTGTGGT	sampleName-n
	
Format all your barcode files in the same manner, in a tab-separated column with no spaces in the sample names. Move these files in the barcodes directory.  
Warning: Do not use special characters (such as: * / \ | $ " etc.)(or spaces for the love of God!) in the names given to your samples in the Barcode file. These may cause sabre to crash.  
The script makeBarcodeSabre.py will be called by the pipeline and will create this file for sabre. The last column now contains the name of the file created by sabre during the demultiplexing phase:  

	GTATTA	Freya_1_samplename.fq
	CTATTA	Freya_1_synergy.fq  
	TAACGA	Freya_1_AC-Metcalfe.fq  
	CGTGTGGT	Freya_1_samplename-n.fq  

these files for each read are then merged 

	CTATTA	Freya_1_synergy.fq	Freya_2_synergy.fq  
	TAACGA	Freya_1_AC-Metcalfe.fq	Freya_2_AC-Metcalfe.fq

## Preparation of the parameters file  

Verify the information in the file parameters.txt. If necessary, change the value given to the variables.  
It is very important not to change the words in capital letters, before the equals(=) sign.  
If you do this, you will get an error message because the pipeline will not find that variable.  
Respect what variables require an integer and a string.  

## Running deepGBSII

To run the pipeline, enter the command: `./deepGBSII.sh parameters.txt`  
To adjust advanced options throughout the deepGBSII script follow the `#OPTION` comments in the code.  

## deepGBSII steps  

###### Initially, the software checks:  
1. if all variables have a value in the parameter file  
2. if a checkpoint file exists.  
Then, it proceeds with the main steps:  
Unecessary data is deleted after the next dependent step completes sucessfully.  
###### Processing steps  
0. Check and if required, copy the flowcell reads 1 and 2 from dataLib (where it is a symbolic link) to data where it will be processed  
1. Production of specific barcodes files with the script makeBarcodeSabre.py  
2. Demultiplex samples with the software sabre  
3. Removing adaptor with cutadapt  
4. Alignment of reads with BWA-MEM  
5. Convert sam files into bam files with samtools  
6. Sort bam files with samtools (optional export)
7. Index bam files with samtools  
8. Production of the file containing the list of bam files to be proces  
9. Search the variants with bcftools  
10. Filter with varying strictness  
11. Impute with beagle  
12. Store the results in a named folder in resultsStored (optional export)

## Checkpoint file  

After each step that completes correctly (exit code 0), the name of the step is written in the checkpoint file.  
The name of this file is your parameter filename appended: checkpoint_$flowcell_$parameterFile
To prevent incorrect pattern matching some of the 'L's are 1(ones), in QC steps.  

	BARCODES  
	SABRE  
	CUTADAPT  
	ALIGN  
	SAMTOBAM  
	SORTBAM  
	BAMLIST  
	INDEXBAM  
	MPILEUP  
	NORMALIZEPILEUPS  
	INDEXPILEUPS  
	MERGEPILEUPS  
	SORTMERGE  
	INDEXMERGE  
	VARIANTCALL  
	VARIANTCA11QC  
	VARIANTFILTER  
	VARIANTFI1TERQC  
	VARIANTVARFILTER  
	VARIANTVARFI1TERQC  
	VARIANTVCFTOOLSFILTER  
	VARIANTVCFTOOLSFI1TERQC  
	IMPUTE  
	EXPORTSPREADSHEET  
	CLEANUP

Once a step name is written in the checkpoint file, if you call the pipeline again, the step will be passed. 
In the same way, if you want to stop processing some steps, for example, you want to do the first steps and check for the results before continuing, 
Write the later step names you don't want to do in the checkpoint file.  
 
## Joblist   

The pipeline assumes you are running paired data, and have named the reads as specified above.  
Once you have made a sucessfull complete run of the piepline in your environment, you may automate multiple plates with the joblist.  
Populate a file with plate names on individual lines.  
	
	Odin  
	Thor  
	Freya

Call the iterator script. 

	./iterator.sh parameters.txt joblist.txt

This will modify the parameters file with sequential lines of the joblist file. 
The dataLib and barcodes directories will be searched for matching plate data. 
Multiple plates may be in these directories and the specific plate will be copied into data to inititate the run. 
The iterator will run the pipeline.	You can use the export functions to make multiple runs without filling the harddrive. 
Creating a dataOP directory and setting a sym link for a complex path across a network will allow a path name with no special characters in the parameters file.  

## Check your files!  

Before running BWA (aligning your reads against a reference genome), the authors of fast-GBS strongly recommend you to count the number of reads in each sample file. Depending on the genome size, the enzyme(s), the sequencing technology and the number of barcodes used, you should have an basic idea on know how much reads you should expect for each sample. If the yield is low and it is not what you may expect, you can ask the sequencing platform to make a new run.  
When you have all your samples demultiplexed, you can run this script to count the number of reads:  

	#!/bin/bash
	cd data
	            for i in $(ls *.fq)
	            do
	                echo $i >> stat_fq_temp.txt
	                cat $i | echo $(($(wc -l)/4)) >> stat_fq_temp.txt
	            done
	perl -pe 's/fq\n/fq\t/' stat_fq_temp.txt > ../control_quality/stat_fq.txt
	
	rm *_temp.txt

It's up to you to determine a threshold for the number of reads accepted. It depends on the length distribution among all your samples. In an nearly ideal situation, you will see the number of reads dropping drastically for a few samples. As a rule of thumb, you can remove samples containing less than 1 or 2% of the number of reads for the sample that contains the maximum.  
To count the number of reads that map or unmap on your reference genome, you can run this scripts:  

	#!/bin/bash
	cd data
	            for i in $(ls *.sort.bam)
	            do
	                echo $i >> stat_mapped_temp.txt
	                echo $i >> stat_unmapped_temp.txt
	                map=$(samtools view -F4 -c $i)
	                unmap=$(samtools view -f4 -c $i)
	                echo $map >> stat_mapped_temp.txt
	                echo $unmap >> stat_unmapped_temp.txt
	            done
	perl -pe 's/bam\n/bam\t/' stat_mapped_temp.txt > ../control_quality/stat_mapped.txt
	perl -pe 's/bam\n/bam\t/' stat_unmapped_temp.txt > ../control_quality/stat_unmapped.txt
	
	rm *_temp.txt
	
## Known bugs or unwant behavior of some softwares  

##### Conda installer  
In some cases the conda installer incorectly solves the environment.
If the mpileup step fails to proceed and the logfile contains the warning

	bcftools: error while loading shared libraries: libcrypto.so.1.0.0: cannot open shared object file: No such file or directory  
	
Solutions exist and have been discussed but not solved for all environments.  
https://github.com/bioconda/bioconda-recipes/issues/12100  
https://github.com/bioconda/bioconda-recipes/issues/13958  
After reading the above, try to explicitly downgrade the openssl version and retry the mpileup step. 
Alternatively, navigate to the conda environment lib directory and create a soft link to redirect the bcftools call using `ln -s libcrypto.so.1.1 libcrypto.so.1.0.0`  
	
	
##### fast-GBS warning 
The cutadapt software may end with code 1 because it produces multiple warnings of this kind:  

	WARNING:  
	    One or more of your adapter sequences may be incomplete.  
	    Please see the detailed output above.
		
This is not an error, just a warning, but it throws the exit code is 1 which is usually used in case of real error.  
Fast-GBS catches the exit code to see if it should end (code 1) or continue (code 0). In this case, the exit code 1 cause Fast-GBS to produce this message and quit :  
	
	!!! There is a problem in the cutadapt step  
	
Just check in the log file to see what happen during the cutadapt step. If all the samples were processed, you can be confident that there is no errors because if a major error happen, the software stop. Habitually, this will happen for a specific sample. If you conclude that there is no problem, you can add manually the name CUTADAP in your checkpoint file and relaunch the pipeline.

##### deepGBSII error handling
An update to this issue. Because of the above issue with cutadapt, cutadapt errors are not caught. 
There is a test after the cutadapt step for a match between input and output types.  
If processing is incomplete an error will be thrown and announced on screen and in logfile and the pipeline will stop.  
The completeness status of processing is also announced

##### Log announcing this manual check after cutadapt  

		printf "\n\tThere may be a problem in the cutadapt step\n" | tee -a ../"${logfile}"
	        printf "\tCheck logfile2_cutadaptSTDERR.txt, if message error  is:\n" | tee -a ../"${logfile}"
	        printf "\t\tOne or more of your adapter sequences may be incomplete.\n" | tee -a ../"${logfile}"
	        printf "\tAnd if all .fq files were processed to .fastq.\n" | tee -a ../"${logfile}"
		printf "\tThis persistent minor error is due to cutadapt's error handling and may be permitted if the adaptor sequences are certain\n"
	        printf "\t!!!EXIT IN ERROR CONDITIONS, other than incomplete file processsing, has been disabled due to this!!! check! \n" 
                        
## OutputStored Directory Contents  

	The 'plateOne' stamp on each file is quite a bit longer, totally unique to a run, and is only applied at program completion to indicate data from a full run and to differentiate data sets. It is bulky but no mixups are possible. It would also self sort if a bunch of runs were thrown in a directory.  
./deepGBSII_plateOne

	These files are the final product, the table.txt is a conversion of the .vcf to be used in excel.
./deepGBSII_plateOne/plateOne_SNP_imputed_table.txt
./deepGBSII_plateOne/plateOne_SNP_imputed.vcf

	These files provide a record of program states for purposes of documentation and reproduction
./deepGBSII_plateOne/states
./deepGBSII_plateOne/states/plateOne_list_bam
./deepGBSII_plateOne/states/plateOne_parameters.txt
./deepGBSII_plateOne/states/plateOne_plateOne_1.unknown.barcodes
./deepGBSII_plateOne/states/plateOne_plateOne_2.unknown.barcodes
./deepGBSII_plateOne/states/plateOne_plateOne_mergedBarcodes
./deepGBSII_plateOne/states/plateOne_codeState_fastGBSII.sh

	There are 4 stages of variants, with associated QC. 
	They file alphabetically in order of processing and (the aim is) in order of filtration strictness.
	The .qstats and .vchk are statistic analysis of the step, respectively with out and with indels included in the analysis.
	There can be a graphic directory per set if LaTeX is installed uncomment the sections noted in QC steps
./deepGBSII_plateOne/variants

	Initial call of variants and indells. Maximum pull, almost no filtration besides a strict single site quality.
./deepGBSII_plateOne/variants/plateOne_variantcall.vcf
./deepGBSII_plateOne/variants/plateOne_variantcall.vcf.qstats
./deepGBSII_plateOne/variants/plateOne_variantcall.vcf.vchk

	Very light filtering for overall quality of snp or indel, and proximity to gaps
./deepGBSII_plateOne/variants/plateOne_variantfilter.vcf
./deepGBSII_plateOne/variants/plateOne_variantfilter.vcf.qstats
./deepGBSII_plateOne/variants/plateOne_variantfilter.vcf.vchk

    Filter out false positives 
./deepGBSII_plateOne/variants/plateOne_variantvarfilter.vcf
./deepGBSII_plateOne/variants/plateOne_variantvarfilter.vcf.qstats
./deepGBSII_plateOne/variants/plateOne_variantvarfilter.vcf.vchk

    Strict Quality filter for biallelc SNP only. This .vcf is imputed 
./deepGBSII_plateOne/variants/plateOne_variantvcftoolsfiltered.recode.vcf
./deepGBSII_plateOne/variants/plateOne_variantvcftoolsfiltered.recode.vcf.qstats
./deepGBSII_plateOne/variants/plateOne_variantvcftoolsfiltered.recode.vcf.vchk

    empty logs are deleted, logs are retained for documentation and debugging.
./deepGBSII_plateOne/logs  
./deepGBSII_plateOne/logs/plateOne_logfile(stepNumber)_(stepName)  
...  
./deepGBSII_plateOne/logs/plateOne_logfile_deepGBSII_plateOne  




