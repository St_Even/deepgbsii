#!/bin/bash

TIMESTAMP=$(date +%Y-%m-%d_%Hh%Mm%Ss)

#get lines from parameters
plate=$(grep "FLOWCELL=" $1 | cut -d "=" -f 2 | sed "s/\r//g")
logfile=$(grep "LOGFILE=" $1 | cut -d "=" -f 2 | sed "s/\r//g")
#test if name was assigned
if [[ -z "${logfile}" ]]
	then
    	printf "\tThe LOGFILE variable does not exist in the parameter file\n"
		exit 1
else
	#build full log name
	logfile="${logfile}_${plate}"
	if [ ! -f "${logfile}" ]
		then

printf "+------------------------------------------------------------+\n" | tee -a "${logfile}"
printf "|           Welcome to deepGBSII for paired data             |\n" | tee -a "${logfile}"
printf "|-._    _.--''''--._    _.--'''''--._    _.--''''--._        |\n" | tee -a "${logfile}"
printf "|'.  '-:'.'|'|'':-.  '-:'.'|'|'':-.  '-:'.'|'|'':-.  '.      |\n" | tee -a "${logfile}"
printf "|  '.  '.  | |  | |'.  '.  | |  | |'.  '.  | |  | |'.  '.    |\n" | tee -a "${logfile}"
printf "|    '.  '.| |  | |  '.  '.| |  | |  '.  '.| |  | |  '.  '.  |\n" | tee -a "${logfile}"
printf "|      '.  '.:_ | :_.' '.  '.:_ | :_.' '.  '.:_ | :_.' '.  '.|\n" | tee -a "${logfile}"
printf "|         '-..,..-'       '-..,..-'       '-..,..-'       '-.|\n" | tee -a "${logfile}"
printf "+------------------------------------------------------------+\n" | tee -a "${logfile}"
printf "            Starting time: ${TIMESTAMP}                       \n" | tee -a "${logfile}"
	else
		printf "\ndeepGBSII>>>>T>>>>A>>>>G>>>>C>> CONTINUE >>T>>>>A>>>>G>>>>C>>>>deepGBSII \n" | tee -a "${logfile}"
		printf "\n The file ${logfile} already exists. Subsequent information will be appended to it\n" | tee -a "${logfile}"
		printf "Starting time: ${TIMESTAMP}\n" | tee -a "${logfile}"
	fi
fi

printf "\nChecking the parameter file\n"  | tee -a "${logfile}"
if [ "$1" != "" ]
	then
	printf "\tThe parameter file is:  ${1}\n"  | tee -a "${logfile}"
else
	printf "\tA parameter file must be specified. ./deepGBSII <parameters.txt>\n\n"  | tee -a "${logfile}"
	exit 1
fi

printf "\nChecking the checkpoint file\n" | tee -a "${logfile}"
if [ ! -f "checkpoint_${plate}" ]
        then
        printf "\tCreating the file checkpoint_${plate}.\n" | tee -a "${logfile}"
        touch "checkpoint_${plate}"
else
        printf "\tThe file checkpoint_${plate} already exists\n"  | tee -a "${logfile}"
fi

printf "\nChecking the variables in the parameter file\n"  | tee -a "${logfile}"

retVal=""
function setVariable {
	#$1 STRING filename of parameters $1
	#$2 STRING variable which will be extracted from parameters
        varNAME=$(echo ${2^^})
        retVal=$(grep "${varNAME}=" $1 | cut -d "=" -f 2 | sed "s/\r//g")

        if [[ -z "${retVal}" ]]
                then
                printf "\tThe ${varNAME} variable does not exist in the parameter file.\n"  | tee -a "${logfile}"
                exit 1
        else
                printf  "\t${varNAME} : ${retVal}\n"  | tee -a "${logfile}"
        fi
}

bamlist="list_bam"

logNum=0

setVariable $1 "flowcell"
flowcell="${retVal}"

setVariable $1 "refgen"
refgen="${retVal}"

setVariable $1 "adaptfwd"
adaptfwd="${retVal}"

setVariable $1 "adaptrev"
adaptrev="${retVal}"

setVariable $1 "nbcor"
nbcor="${retVal}"

setVariable $1 "bwapar"
bwapar="${retVal}"

setVariable $1 "bwathr"
bwathr="${retVal}"

setVariable $1 "temp"
temp="${retVal}"

setVariable $1 "offLoad"
offLoad="${retVal}"

setVariable $1 "offLoc"
offLoc="${retVal}"

setVariable $1 "output"
output="${retVal}"

setVariable $1 "outLoc"
outLoc="${retVal}"

echo =========================================================

function offLoadFN {

	type="${1}"
	name="${flowcell}_${TIMESTAMP}_"

	#test if offLoc requires to be made
	if [ ! -d "${offLoc}" ]
		then
		printf "\nCreating the Directory OFFLOC: ${offLoc}\n\n" | tee -a ../"${logfile}"
		mkdir -p "${offLoc}"
	fi
	#test if function is being used
	if [ "${offLoad}" == "YES" ]
		then
		printf "\tCopying *${type} files to OFFLOC: ${offLoc}\n\n" | tee -a ../"${logfile}"

		#copy
		list="$(ls -1 *${type})"
		parallel -j 80% \
                        rsync -vPgo -T "/tmp" {} "${offLoc}" \
                ::: "${list}"

		#rename
		here="$(pwd)"
		cd "${offLoc}"
                parallel -j 80% \
                        mv {} "${name}"{} \
                ::: "${list}"
		cd "${here}"

	fi
}

function outputFN {
#call from program level
#...this function is not debugged
        expDir="${1}"
	#test if outLoc exists
        if [ -d "${outLoc}" ]
                then
		#test if function is being used
                if [ "${output}" == "YES" ]
                        then
                        printf "\tCopying resultsStored/${expDir} to OUTLOC: ${outLoc}\n\n" | tee -a "${logfile}"
                        #copy
                        cd resultsStored
			rsync -vPrgo -T "/tmp" "${expDir}" "${outLoc}"
                        #test exit status
		        if [ $? -e 0 ]
                                then
                                #remove
                                printf "\tTest:outputFN this step would remove resultsStored/${expDir}\n" | tee -a ../"${logfile}"
                                #rm -r "${expDir}"
                                cd ..
                        else
                                printf "\tThere was a problem copying the files from resultsStored.\n" | tee -a ../"${logfile}"
				exit 1
                                cd ..
                        fi
                fi
	else
                printf "\tThe directory: ${outLoc} does not exist, the resultsStored will not be exported\n" | tee -a "${logfile}"
        fi
}

function testProcessed {
        intype="$1"
        outtype="$2"
	#default intype trim
        ls *"${intype}" | sed "s:${intype}::" > listin
	#.fastq intype trim
	if [ "${intype}" == ".fastq" ]
		then
		echo "$(ls -1 *.fastq | sed 's/.fastq//' | grep _1_ | sed "s:${flowcell}_[12]_::g")" > listin
	fi
        ls *"${outtype}" | sed "s:${outtype}::" > listout
        differences=$(diff -q listin listout)
        rm listin
        rm listout
        if [ "${differences}" != "" ]
                then
                printf "\tERROR Not all ${intype} files were processed to ${outtype}\n" | tee -a ../"${logfile}"
                exit 1
        else
                printf "\tAll ${intype} files were processed to ${outtype}\n" | tee -a ../"${logfile}"
                rm *"${intype}"
        fi
}

function testExit {
	#$1 INT exit status $?
	#$2 STRING step name $stepName
	if [ $1 -ne 0 ]
                then
                printf "\tThere is a problem in the ${2} call\n" | tee -a ../"${logfile}"
                exit 1
        else
                printf "\tCompleted the ${2} call\n" | tee -a ../"${logfile}"
        fi
}

echo =========================================================
#copy reads from dataLib to data

#test if reads have been demultiplexed
Step=$(grep "SABRE" checkpoint_${plate})
if [ "${Step}" != "SABRE" ]
        then

	cd data
	#test if the data is already present
	if [ -e ${flowcell}_1.fq.gz ] && [ -e ${flowcell}_2.fq.gz ]
		then
		printf "\tContents of the data dir:\n" | tee -a ../"${logfile}"
                printf "$(ls *)\n" | tee -a ../"${logfile}"
		cd ..
	#copying the data is required
	else
		cd ../dataLib
		printf "\nCopying ${flowcell} from datalib to data\n" | tee -a ../"${logfile}"
		parallel -j 80% \
			rsync -vaLP {} ../data/ \
		::: "$(ls ${flowcell}*)"
		cd ..
	fi
#demultiplexing has already occured
else
	cd data
	printf "\nIt is not required to copy the reads into data.\n\n" | tee -a ../"${logfile}"
	cd ..
fi



echo ===========================================================

stepName="barcodes"

stepNAME=$(echo ${stepName^^})
stepGet=$(grep "${stepNAME}" checkpoint_${plate})
if [ "${stepGet}" != "${stepNAME}" ]
        then
        printf "\nBeginning step: ${stepName}\n" | tee -a "${logfile}"

	printf "\tList of flowcell and their lanes" | tee -a "${logfile}"
	for f in ${flowcell}
	do
		printf "\n\t${f}\n" | tee -a "${logfile}"
		lanes=$(grep "${f}_LANES=" ${1} | cut -d "=" -f 2 | sed "s/\r//g")
		for l in ${lanes}
		do
			printf "\t\t${l}\n" | tee -a "${logfile}"
			./makeBarcodeSabre.py ${f} ${l}
			if [ $? -ne 0 ]
				then
				printf "\t!!! There is a problem in step makeBarcode with ${f} ${l}\n" | tee -a "${logfile}"
				printf "\t!!! Check the names given to the barcode files in the barcode directory"  | tee -a "${logfile}"
				exit 1
			fi
		done

		#merge the barcode files
		cd data
		paste -d ',' ${flowcell}_1 ${flowcell}_2 > ${flowcell}_merged
		sed -i 's:,[TAGC]*::g' ${flowcell}_merged
		sed -i "s: ${flowcell}:\t${flowcell}:g" ${flowcell}_merged

		testExit "$?" "${stepName}"

		cd ..
	done

        printf "${stepNAME}\n" >> checkpoint_${plate}
else
        printf "\t${stepNAME} is in the checkpoint file. This step will be passed\n"| tee -a "${logfile}"
fi

echo ========================================================================

stepName="sabre"

((logNum++))
stepNAME=$(echo ${stepName^^})
stepGet=$(grep "${stepNAME}" checkpoint_${plate})
if [ "${stepGet}" != "${stepNAME}" ]
        then
        printf "\nBeginning step: ${stepName}\n" | tee -a "${logfile}"

	cd data

			#OPTION
			#-m <INT> allows for mismatches
        	        #-c remove barcodes from both files
	parallel -j "${nbcor}" \
		sabre pe \
			-c \
			-m 2 \
			-f {}_1.fq.gz \
			-r {}_2.fq.gz \
			-b {}_merged \
			-u {}_1.unknown.barcodes \
			-w {}_2.unknown.barcodes \
			&> ../"logfile${logNum}_${stepName}" \
	::: "${flowcell}"

	testExit "$?" "${stepName}"

		rm *.fq.gz
		rm "${flowcell}"_[12]
		mv "${flowcell}"_merged "${flowcell}"_mergedBarcodes
		mv "${flowcell}"_mergedBarcodes ../results
		mv *unknown* ../results

	cd ..

        printf "${stepNAME}\n" >> checkpoint_${plate}

else
        printf "\t${stepNAME} is in the checkpoint file. This step will be passed\n" | tee -a "${logfile}"
fi

echo ========================================================================

stepName="cutAdapt"

((logNum++))
stepNAME=$(echo ${stepName^^})
stepGet=$(grep "${stepNAME}" checkpoint_${plate})
if [ "${stepGet}" != "${stepNAME}" ]
        then
        printf "\nBeginning step: ${stepName}\n" | tee -a "${logfile}"

		cd data
				#OPTION
				#--minimum-length <INT> Discard processed reads that are shorter than LENGTH. If you do not use this option, reads that have a length of zero (empty reads) are kept in the output. Zero length reads may cause problems downstream
				#https://cutadapt.readthedocs.io/en/v1.8.2/guide.html#basic-usage
		fqlist=$(ls -1 *.fq | sed 's/.fq//' | grep _1_ | sed "s:${flowcell}_[12]_::g" )
		parallel -j 80% \
			cutadapt \
				-a ${adaptfwd} \
				-A ${adaptrev} \
				--minimum-length 20 \
				-o "${flowcell}"_1_{}.fastq \
				-p "${flowcell}"_2_{}.fastq \
				"${flowcell}"_1_{}.fq \
				"${flowcell}"_2_{}.fq \
				2> ../"logfile${logNum}_${stepName}STDERR" \
		::: "${fqlist}"

		#exit on error has been disabled
		if [ $? -ne 0 ]
			then
			printf "\n\tThere may be a problem in the cutadapt step\n" | tee -a ../"${logfile}"
			printf "\tCheck logfile2_cutadaptSTDERR.txt, if the error message reads:\n" | tee -a ../"${logfile}"
			printf "\t\tOne or more of your adapter sequences may be incomplete.\n" | tee -a ../"${logfile}"
			printf "\tAnd if all .fq files were processed to .fastq.\n" | tee -a ../"${logfile}"
			printf "\tThis persistent minor error is due to cutadapt's error handling and may be permitted if the adaptor sequences are certain\n" | tee -a ../"${logfile}"
			printf "\t!!!EXIT IN ERROR CONDITIONS, other than incomplete file processsing, has been disabled due to this!!! check the logfile! \n" | tee -a ../"${logfile}"
			#exit 1
		fi

	#test if all files have been processed by this stage
        intype=".fq"
        outtype=".fastq"
	testProcessed "${intype}" "${outtype}"

	cd ..

        printf "${stepNAME}\n" >> checkpoint_${plate}

else
        printf "\t${stepNAME} is in the checkpoint file. This step will be passed\n" | tee -a "${logfile}"
fi

echo ========================================================================

stepName="align"

((logNum++))
stepNAME=$(echo ${stepName^^})
stepGet=$(grep "${stepNAME}" checkpoint_${plate})
if [ "${stepGet}" != "${stepNAME}" ]
        then
        printf "\nBeginning step: ${stepName}\n" | tee -a "${logfile}"

	cd data

	fastqlist=$(ls -1 *.fastq | sed 's/.fastq//' | grep _1_ | sed "s:${flowcell}_[12]_::g" )
	parallel -j "${bwapar}" \
		bwa mem \
			-t "${bwathr}" \
			../refgenome/"${refgen}" \
			"${flowcell}"_1_{}.fastq \
			"${flowcell}"_2_{}.fastq \
			">" {}.sam \
			&> ../"logfile${logNum}_${stepName}" \
	::: "${fastqlist}"

	testExit "$?" "${stepName}"

	#test if all files have been processed by this stage
	intype=".fastq"
	outtype=".sam"
	testProcessed "${intype}" "${outtype}"

	cd ..

        printf "${stepNAME}\n" >> checkpoint_${plate}
else
        printf "\t${stepNAME} is in the checkpoint file. This step will be passed\n" | tee -a "${logfile}"
fi

echo ========================================================================

stepName="samToBam"

((logNum++))
stepNAME=$(echo ${stepName^^})
stepGet=$(grep "${stepNAME}" checkpoint_${plate})
if [ "${stepGet}" != "${stepNAME}" ]
        then
        printf "\nBeginning step: ${stepName}\n" | tee -a "${logfile}"

	cd data
	parallel -j "${nbcor}" \
		samtools view -bSh \
			{}.sam \
			">" {}.temp.bam \
			2> ../"logfile${logNum}_${stepName}"STDERR \
	::: $(ls -1 *.sam | sed 's/.sam//')

	testExit "$?" "${stepName}"

	#test if all files have been processed by this stage
        intype=".sam"
        outtype=".temp.bam"
	testProcessed "${intype}" "${outtype}"

	cd ..

        printf "${stepNAME}\n" >> checkpoint_${plate}
else
        printf "\t${stepNAME} is in the checkpoint file. This step will be passed\n" | tee -a "${logfile}"
fi

echo ========================================================================

stepName="sortBam"

((logNum++))
stepNAME=$(echo ${stepName^^})
stepGet=$(grep "${stepNAME}" checkpoint_${plate})
if [ "${stepGet}" != "${stepNAME}" ]
        then
        printf "\nBeginning step: ${stepName}\n" | tee -a "${logfile}"

	cd data
	parallel -j "${nbcor}" \
		samtools sort \
			{}.temp.bam \
			-o {}.sort.bam \
			2> ../"logfile${logNum}_${stepName}"STDERR \
	::: $(ls -1 *.temp.bam | sed 's/.temp.bam//')

	testExit "$?" "${stepName}"

        intype=".temp.bam"
        outtype=".sort.bam"
	testProcessed "${intype}" "${outtype}"

	cd ..

        printf "${stepNAME}\n" >> checkpoint_${plate}
else
        printf "\t${stepNAME} is in the checkpoint file. This step will be passed\n" | tee -a "${logfile}"
fi

echo ========================================================================

stepName="bamList"


stepNAME=$(echo ${stepName^^})
stepGet=$(grep "${stepNAME}" checkpoint_${plate})
if [ "${stepGet}" != "${stepNAME}" ]
        then
        printf "\nBeginning step: ${stepName}\n" | tee -a "${logfile}"

	cd data

	if [ ! -f "${bamlist}" ]
		then
		printf "\tThe file ${bamlist} does not exist. It will be created.\n" | tee -a ../"${logfile}"
		touch "${bamlist}"
	else
		printf "\tThe file ${bamlist} already exist! It will be reset.\n" | tee -a ../"${logfile}"
		rm "${bamlist}"
		touch "${bamlist}"
	fi

	for i in $(ls -1 *.sort.bam)
		do
		printf "$PWD/${i}\n" >> "${bamlist}"
		if [ $? -ne 0 ]
			then
			printf "\tThere is a problem with $i in the production of the bam file list\n" | tee -a ../"${logfile}"
			exit 1
		fi
	done

	cd ..

        printf "${stepNAME}\n" >> checkpoint_${plate}
else
        printf "\t${stepNAME} is in the checkpoint file. This step will be passed\n" | tee -a "${logfile}"
fi

echo ========================================================================

#create .cai read index, needed by some processes
stepName="indexBam"

((logNum++))
stepNAME=$(echo ${stepName^^})
stepGet=$(grep "${stepNAME}" checkpoint_${plate})
if [ "${stepGet}" != "${stepNAME}" ]
        then
        printf "\nBeginning step: ${stepName}\n" | tee -a "${logfile}"

        cd data

        parallel -j "${nbcor}" \
                samtools index -c {} \
                2> ../"logfile${logNum}_${stepName}"STDERR \
        ::: $(ls -1 *.sort.bam)

	testExit "$?" "${stepName}"

        cd ..

	printf "${stepNAME}\n" >> checkpoint_${plate}
else
        printf "\t${stepNAME} is in the checkpoint file. This step will be passed\n" | tee -a "${logfile}"
fi

# up to this point the pipeline has adhered to the fastGBS pipeline, adding paired functionality, file handling and logging
# For large plant genomes, the sam files cannot be indexed by a method (.bai) which platypus will accept.

echo ========================================================================

# this paper supports the design of the pipeline beyond this point, with respect to calling variants on  large plant genomes.
#Yao, Z., You, F.M., N’Diaye, A. et al. Evaluation of variant calling tools for large plant genome re-sequencing. BMC Bioinformatics 21, 360 (2020). https://doi.org/10.1186/s12859-020-03704-1

stepName="mpileup"

((logNum++))
stepNAME=$(echo ${stepName^^})
stepGet=$(grep "${stepNAME}" checkpoint_${plate})
if [ "${stepGet}" != "${stepNAME}" ]
        then
        printf "\nBeginning step: ${stepName}\n" | tee -a "${logfile}"

	cd data

	#sanity check files
	printf "\tChecking the .sort.bam files with quickcheck\n" | tee -a ../"${logfile}"
	samtools quickcheck -v *.sort.bam
	if [ $? == 0 ]
	        then
		printf "\tThe .sort.bam files appear good. Proceed with mpileup\n" | tee -a ../"${logfile}"
	else
		printf "\tThere is a problem with the .sort.bam files\n" | tee -a ../"${logfile}"
	        exit 1
	fi

	#for each bam file run mpileup then delete, optional offload
        bam="$(ls -1 *.sort.bam | sed "s:.sort.bam::")"
        for i in $bam
                do
			#OPTION
	                #--min-BQ INT Minimum base quality for a base to be considered [13]
        	        #--maxdepth positively-correlates with processing time and quality of result. default 250 max 8000
			#https://samtools.github.io/bcftools/bcftools.html#mpileup
                bcftools mpileup \
                        --max-depth 8000 \
                        --redo-BAQ \
                        --min-BQ 30 \
                        --per-sample-mF \
                        --annotate FORMAT/AD,FORMAT/ADF,FORMAT/ADR,FORMAT/DP,FORMAT/SP,INFO/AD,INFO/ADF,INFO/ADR \
                        -O b \
                        -o "$i".mpileup.bcf \
                        -f ../refgenome/"${refgen}" \
                        "$i".sort.bam \
                        &> ../"logfile${logNum}_${stepName}" \

		testExit "$?" "${stepName}"
		printf "\tfor ${i}\n" | tee -a ../"${logfile}"
			#store specific sort.bam
			offLoadFN "${i}.sort.bam"
			#remove sort.bam and index
			rm "$i".sort.bam
			rm "$i".sort.bam.csi
	done

        cd ..

        printf "${stepNAME}\n" >> checkpoint_${plate}
else
        printf "\t${stepNAME} is in the checkpoint file. This step will be passed\n" | tee -a "${logfile}"
fi

echo ========================================================================
#normalize variants. per discussion: https://genome.sph.umich.edu/wiki/Variant_Normalization
stepName="normalizePileups"

((logNum++))
stepNAME=$(echo ${stepName^^})
stepGet=$(grep "${stepNAME}" checkpoint_${plate})
if [ "${stepGet}" != "${stepNAME}" ]
        then
        printf "\nBeginning step: ${stepName}\n" | tee -a "${logfile}"

        cd data

			#OPTION
			#--check-ref e|w|x|s Sets action when incorrect or missing REF allele is encountered: exit (e), warn (w),exclude (x), or set/fix (s) bad sites. The w option can be combined with x and s. Note that s can swap alleles and will update genotypes (GT) and AC counts, but will not attempt to fix PL or other fields.Also note, and this cannot be stressed enough, that s will NOT fix strand issues in your VCF,do NOT use it for that purpose!!!
			#--rm-dup exact remove duplicates. Only records with identical REF and ALT alleles are compatible to be collapsed. norm documentaion is in error argument is "none" see docs for "collapse".
			#https://samtools.github.io/bcftools/bcftools.html#norm
	parallel -j 80% \
		bcftools norm \
			--check-ref wx \
			--rm-dup none \
			-f ../refgenome/"${refgen}" \
			-O b \
			-o {}.mpileup.norm.bcf \
			{}.mpileup.bcf \
			&> ../"logfile${logNum}_${stepName}" \
	::: $(ls -1 *.mpileup.bcf | sed 's:.mpileup.bcf::')

	testExit "$?" "${stepName}"

		rm *.mpileup.bcf
        cd ..

        printf "${stepNAME}\n" >> checkpoint_${plate}
else
        printf "\t${stepNAME} is in the checkpoint file. This step will be passed\n" | tee -a "${logfile}"
fi

echo ========================================================================

stepName="indexPileups"

((logNum++))
stepNAME=$(echo ${stepName^^})
stepGet=$(grep "${stepNAME}" checkpoint_${plate})
if [ "${stepGet}" != "${stepNAME}" ]
        then
        printf "\nBeginning step: ${stepName}\n" | tee -a "${logfile}"

        cd data

	parallel -j 80% \
		bcftools index \
			-c \
			-f \
			{} \
			&> ../"logfile${logNum}_${stepName}" \
	::: $(ls -1 *.mpileup.norm.bcf)

	testExit "$?" "${stepName}"

        cd ..

        printf "${stepNAME}\n" >> checkpoint_${plate}
else
        printf "\t${stepNAME} is in the checkpoint file. This step will be passed\n" | tee -a "${logfile}"
fi

echo ========================================================================


stepName="mergePileups"

((logNum++))
stepNAME=$(echo ${stepName^^})
stepGet=$(grep "${stepNAME}" checkpoint_${plate})
if [ "${stepGet}" != "${stepNAME}" ]
        then
        printf "\nBeginning step: ${stepName}\n" | tee -a "${logfile}"

        cd data

	#create list of files to merge
        cat "${bamlist}" | sed 's:.sort.bam:.mpileup.norm.bcf:g' > bcflist
	#merge the mpileup output
        bcftools merge \
                --file-list bcflist \
                --merge both \
                -O b \
                -o mpileup.bcf \
                &> ../"logfile${logNum}_${stepName}"

	testExit "$?" "${stepName}"

                rm *.mpileup.norm.bcf*
		rm bcflist

	cd ..

        printf "${stepNAME}\n" >> checkpoint_${plate}
else
        printf "\t${stepNAME} is in the checkpoint file. This step will be passed\n" | tee -a "${logfile}"
fi

echo ========================================================================

stepName="sortMerge"

((logNum++))
stepNAME=$(echo ${stepName^^})
stepGet=$(grep "${stepNAME}" checkpoint_${plate})
if [ "${stepGet}" != "${stepNAME}" ]
        then
        printf "\nBeginning step: ${stepName}\n" | tee -a "${logfile}"

	#create temp if necessary
	if [ ! -e "${temp}" ]
		then
		printf "\tCreating temp directory: ${temp}\n" | tee -a "${logfile}"
		mkdir -p "${temp}"
	fi

        cd data

	#sort, just in case
	bcftools sort \
		--temp-dir "${temp}" \
                -O b \
                -o mpileup.sort.bcf \
                mpileup.bcf \
                &> ../"logfile${logNum}_${stepName}"

	testExit "$?" "${stepName}"

                rm mpileup.bcf
		rm -r ../temp
		mkdir ../temp

        cd ..

        printf "${stepNAME}\n" >> checkpoint_${plate}
else
        printf "\t${stepNAME} is in the checkpoint file. This step will be passed\n" | tee -a "${logfile}"
fi

echo ========================================================================

stepName="indexMerge"

((logNum++))
stepNAME=$(echo ${stepName^^})
stepGet=$(grep "${stepNAME}" checkpoint_${plate})
if [ "${stepGet}" != "${stepNAME}" ]
        then
        printf "\nBeginning step: ${stepName}\n" | tee -a "${logfile}"

        cd data

	#index the merged mpileup
	bcftools index \
                -c \
		-f \
                mpileup.sort.bcf \
                &> ../"logfile${logNum}_${stepName}"

	testExit "$?" "${stepName}"

        cd ..

        printf "${stepNAME}\n" >> checkpoint_${plate}
else
        printf "\t${stepNAME} is in the checkpoint file. This step will be passed\n" | tee -a "${logfile}"
fi

echo ========================================================================

stepName="variantCall"

((logNum++))
stepNAME=$(echo ${stepName^^})
stepGet=$(grep "${stepNAME}" checkpoint_${plate})
if [ "${stepGet}" != "${stepNAME}" ]
        then
        printf "\nBeginning step: ${stepName}\n" | tee -a "${logfile}"

	cd data

		#OPTION
		#--prior expected substitution rate, or 0 to disable.default is 1.1e-3
			# Smaller values are stricter, Values above actual increase false positives, values below actual exclude results
			#https://www.genetics.org/content/162/3/1389 supports -p value of 4.2 × 10-9 for barley
		#https://samtools.github.io/bcftools/bcftools.html#call
	bcftools call \
		--multiallelic-caller \
		--variants-only \
		--prior 4.2e-9 \
        	-O v \
		-o variantcall.vcf \
		mpileup.sort.bcf \
		&> ../"logfile${logNum}_${stepName}"

	testExit "$?" "${stepName}"
		rm mpileup.sort.bcf*
#		mv mpileup.sort.bcf* ../results
		mv list_bam ../results

	cd ..

        printf "${stepNAME}\n" >> checkpoint_${plate}
else
        printf "\t${stepNAME} is in the checkpoint file. This step will be passed\n" | tee -a "${logfile}"
fi

echo ========================================================================
#these '11' are ones to prevent pattern matching prev step
stepName="variantCa11QC"

((logNum++))
stepNAME=$(echo ${stepName^^})
stepGet=$(grep "${stepNAME}" checkpoint_${plate})
if [ "${stepGet}" != "${stepNAME}" ]
        then
        printf "\nBeginning step: ${stepName}\n" | tee -a "${logfile}"

        cd data

	#specify reference variables, for comparison and cleanup
	#${refStats} is moved before beagle
	refStats="variantcall.vcf.vchk"
	refStatsDir="variantcall.vcf"


	#specify step variables
	vcf="variantcall.vcf"
	dir=$(echo $vcf | sed 's:.vcf::')
	#quality stats for human readers
	printf "Begin Qstats\n\n" > ../"logfile${logNum}_${stepName}"STDERR
	#create a header
	printf "\nNote: This command discards indels." > ${vcf}.qstats
	printf "\nOUTPUT\n" >> ${vcf}.qstats
	printf "\nQUAL\n\tnon-indel\n\t\tSNPs\n\t\t\ttransitions\n\t\t\t\tjoint\n\t\t\t\t\tts/tv\n\t\t\t\t\t\tjoint/ref\n\t\t\t\t\t\t\tjoint/non-indel\n" >> ${vcf}.qstats
	vcfutils.pl qstats \
		${vcf} \
		>> ${vcf}.qstats \
		2>> ../"logfile${logNum}_${stepName}"STDERR
	#visualize quality stats
	printf "\n\nBegin Bcftools Stats\n\n" >> ../"logfile${logNum}_${stepName}"STDERR
	bcftools stats \
		-F ../refgenome/"${refgen}" \
		${vcf} \
		> ${vcf}.vchk \
		2>> ../"logfile${logNum}_${stepName}"STDERR
#...needs install of latex to generate pdfs
#	printf "\n\nBegin Plot-VcfStats\n\n" >> ../"logfile${logNum}_${stepName}"STDERR
#	plot-vcfstats \
#		-p $(pwd)/${dir} \
#		--main-title "Unfiltered Variants" \
#		${vcf}.vchk \
#		2>> ../"logfile${logNum}_${stepName}"STDERR
	testExit "$?" "${stepName}"
#		mv ${vcf}.qstats ./${dir}
#		mv ${vcf}.vchk ./${dir}

	cd ..

        printf "${stepNAME}\n" >> checkpoint_${plate}
else
        printf "\t${stepNAME} is in the checkpoint file. This step will be passed\n" | tee -a "${logfile}"
fi


echo ========================================================================

<<COMMENT
BEGIN

Direct imputation of SNP Call

the following documents the environment used to process a direct imputation of the variantcall.vcf

these parameters are not optimised but they have worked, 
generally I set them by doubling or halving from the last point of failure.
reducing the window to 20 was necessary to complete the imputation on plate1 chr6 without throwing java memory heap size errors 
ditto                  10                         ditto               plate4 chr4            ditto
the memory requirements of qsub v_mem and java Xmx can likely be reduced, they were serially doubled to their current setting, before the window size was reduced.

qsub script for AAFC biocluster: 40 cores with 64G of ram each
===========================
#!/bin/bash

#$ -S /bin/bash
#$ -N RunDeepGBSII
#$ -pe smp 40
#$ -l h_vmem=64G
#$ -cwd
#$ -m ea
#$ -M your@email.com
#$ -w e

conda activate deepGBS
./deepGBSII.sh parameters.txt
==============================


modified default java memory settings

================================
cd  ~/miniconda3/envs/deepGBS/bin
nano beagle 
#set this line for a 512G heap max
default_jvm_mem_opts="-Xms512m -Xmx512g"
================================

END
COMMENT



stepName="directImpute"
printf "\t${stepNAME} step may require the processing environment to be modified to satisfy memory requirements, see comment beginning line 895.\n" | tee -a "${logfile}"

((logNum++))
stepNAME=$(echo ${stepName^^})
stepGet=$(grep "${stepNAME}" checkpoint_${plate})
if [ "${stepGet}" != "${stepNAME}" ]
        then
        printf "\nBeginning step: ${stepName}\n" | tee -a "${logfile}"

        cd data
	
	#OPTION
	#window (default 40) must be larger that 1.1x4.0 (default overlap)
	for chr in {chr1H,chr2H,chr3H,chr4H,chr5H,chr6H,chr7H,chrUn}
        do
        echo "imputing CHR $chr" | tee -a ../"${logfile}"

        beagle \
                gt=variantcall.vcf \
                nthreads="${nbcor}" \
                chrom=${chr} \
				window=10.0 \
                out=${plate}_CHR${chr}_imputed \
                &> ../"log_${plate}_CHR${chr}_${stepName}"

		testExit "$?" "${stepName}"
	done
	
        cd ..

        printf "${stepNAME}\n" >> checkpoint_${plate}
else
        printf "\t${stepNAME} is in the checkpoint file. This step will be passed\n" | tee -a "${logfile}"
fi

echo ========================================================================





<<SKIP
BEGIN

beyond this point the design is viable but the filter setting require adjustment.
We found the filtering as set retained excessive heterozygotic results ~ 0.3-0.4

echo ========================================================================

<<COMMENT
BEGIN


mpileup			*.mpileup.bcf
	norm		*.mpileup.norm.bcf
	index		*.mpileup.norm.bcf.csi
	merge		mpileup.bcf
	sort		mpileup.sort.bcf
	index		mpileup.sort.bcf.csi
			**These retained results sort in alphabetic and processing order.
bcftools call		variantcall.vcf
bcftools filter 	variantfilter.vcf
vcfutils.pl varfilter	variantvarfilter.vcf
vcftools		variantvcftoolsfiltered.vcf.recode...
impute			variant_imputed.vcf
exp 			...


END OF
COMMENT
echo ========================================================================


stepName="variantFilter"
#Intent: remove low quality results, lowpass filter

((logNum++))
stepNAME=$(echo ${stepName^^})
stepGet=$(grep "${stepNAME}" checkpoint_${plate})
if [ "${stepGet}" != "${stepNAME}" ]
        then
        printf "\nBeginning step: ${stepName}\n" | tee -a "${logfile}"

        cd data

        #OPTION
	#--SnpGap INT[:indel,mnp,bnd,other,overlap] filter SNPs within INT base pairs of an indel or other other variant type.
	#--IndelGap INT filter clusters of indels separated by INT or fewer base pairs allowing only one to pass.
	#https://samtools.github.io/bcftools/bcftools.html#filter
	bcftools filter \
		--threads "${numcor}" \
		--soft-filter "LowQual" \
		--SnpGap 3 \
		--IndelGap 10 \
		--exclude '%QUAL<10' \
		-O v \
		-o variantfilter.vcf \
		variantcall.vcf \
		&> ../"logfile${logNum}_${stepName}"

	testExit "$?" "${stepName}"

	cd ..

	printf "${stepNAME}\n" >> checkpoint_${plate}
else
        printf "\t${stepNAME} is in the checkpoint file. This step will be passed\n" | tee -a "${logfile}"
fi

echo ========================================================================
#this 'l' is a one to prevent pattern matching
stepName="variantFi1terQC"

((logNum++))
stepNAME=$(echo ${stepName^^})
stepGet=$(grep "${stepNAME}" checkpoint_${plate})
if [ "${stepGet}" != "${stepNAME}" ]
        then
        printf "\nBeginning step: ${stepName}\n" | tee -a "${logfile}"

        cd data

	#specify step variables
        vcf="variantfilter.vcf"
        dir=$(echo $vcf | sed 's:.vcf::')
        #quality stats for human readers
        printf "Begin Qstats\n\n" > ../"logfile${logNum}_${stepName}"STDERR
        #create a header
        printf "\nNote: This command discards indels." > ${vcf}.qstats
        printf "\nOUTPUT\n" >> ${vcf}.qstats
	printf "\nQUAL\n\tnon-indel\n\t\tSNPs\n\t\t\ttransitions\n\t\t\t\tjoint\n\t\t\t\t\tts/tv\n\t\t\t\t\t\tjoint/ref\n\t\t\t\t\t\t\tjoint/non-indel\n" >> ${vcf}.qstats
        vcfutils.pl qstats \
                ${vcf} \
                >> ${vcf}.qstats \
                2>> ../"logfile${logNum}_${stepName}"STDERR
        #visualize quality stats
        printf "\n\nBegin Bcftools Stats\n\n" >> ../"logfile${logNum}_${stepName}"STDERR
        bcftools stats \
                -F ../refgenome/"${refgen}" \
                ${vcf} \
                > ${vcf}.vchk \
                2>> ../"logfile${logNum}_${stepName}"STDERR
#...needs install of latex to generate pdfs
#       printf "\n\nBegin Plot-VcfStats\n\n" >> ../"logfile${logNum}_${stepName}"STDERR
#       plot-vcfstats \
#               -p $(pwd)/${dir} \
#               --main-title "Unfiltered Variants" \
#               ${vcf}.vchk \
#               2>> ../"logfile${logNum}_${stepName}"STDERR
        testExit "$?" "${stepName}"
#               mv ${vcf}.qstats ./${dir}
#               mv ${vcf}.vchk ./${dir}

        cd ..

        printf "${stepNAME}\n" >> checkpoint_${plate}
else
        printf "\t${stepNAME} is in the checkpoint file. This step will be passed\n" | tee -a "${logfile}"
fi

echo ========================================================================

stepName="variantVarFilter"
#Intent: remove false positives, eliminate a subset.

((logNum++))
stepNAME=$(echo ${stepName^^})
stepGet=$(grep "${stepNAME}" checkpoint_${plate})
if [ "${stepGet}" != "${stepNAME}" ]
        then
        printf "\nBeginning step: ${stepName}\n" | tee -a "${logfile}"

        cd data

	#OPTION
        #   -Q INT  minimum RMS mapping quality for SNPs [10]
        #   -d INT  minimum read depth [2]
        #   -D INT  maximum read depth [10000000]
        #   -a INT  minimum number of alternate bases [2]
        #   -w INT  SNP within INT bp around a gap to be filtered [3]
        #   -W INT  window size for filtering adjacent gaps [10]
        #   -1 FLOAT    min P-value for strand bias (given PV4) [0.0001]
        #   -2 FLOAT    min P-value for baseQ bias [1e-100]
        #   -3 FLOAT    min P-value for mapQ bias [0]
        #   -4 FLOAT    min P-value for end distance bias [0.0001]
        #   -e FLOAT    min P-value for HWE (plus F<0) [0.0001]
        #   -p      print filtered variants
	#docs not yet avail?   https://sourceforge.net/p/samtools/mailman/message/26610448/
        vcfutils.pl varFilter \
                -d 18 \
                -w 1 \
                -W 3 \
                -a 1 \
                -1 0.05 \
                -2 0.05 \
                -3 0.05 \
                -4 0.05 \
                -e 0.05 \
                -p \
		> variantvarfilter.vcf \
		variantfilter.vcf \
		2> ../"logfile${logNum}_${stepName}"STDERR

	testExit "$?" "${stepName}"

        cd ..

        printf "${stepNAME}\n" >> checkpoint_${plate}
else
        printf "\t${stepNAME} is in the checkpoint file. This step will be passed\n" | tee -a "${logfile}"
fi

echo ========================================================================

#'l' is one to prevent pattern matching
stepName="variantVarFi1terQC"

((logNum++))
stepNAME=$(echo ${stepName^^})
stepGet=$(grep "${stepNAME}" checkpoint_${plate})
if [ "${stepGet}" != "${stepNAME}" ]
        then
        printf "\nBeginning step: ${stepName}\n" | tee -a "${logfile}"

        cd data

	#specify step variables
	vcf="variantvarfilter.vcf"
        dir=$(echo $vcf | sed 's:.vcf::')

        #quality stats for human readers
        printf "Begin Qstats\n\n" > ../"logfile${logNum}_${stepName}"STDERR
        #create a header
        printf "\nNote: This command discards indels." > ${vcf}.qstats
        printf "\nOUTPUT\n" >> ${vcf}.qstats
	printf "\nQUAL\n\tnon-indel\n\t\tSNPs\n\t\t\ttransitions\n\t\t\t\tjoint\n\t\t\t\t\tts/tv\n\t\t\t\t\t\tjoint/ref\n\t\t\t\t\t\t\tjoint/non-indel\n" >> ${vcf}.qstats
        vcfutils.pl qstats \
                ${vcf} \
                >> ${vcf}.qstats \
                2>> ../"logfile${logNum}_${stepName}"STDERR
        #visualize quality stats
        printf "\n\nBegin Bcftools Stats\n\n" >> ../"logfile${logNum}_${stepName}"STDERR
        bcftools stats \
                -F ../refgenome/"${refgen}" \
                ${vcf} \
                > ${vcf}.vchk \
                2>> ../"logfile${logNum}_${stepName}"STDERR
#...needs install of latex to generate pdfs
#       printf "\n\nBegin Plot-VcfStats\n\n" >> ../"logfile${logNum}_${stepName}"STDERR
#       plot-vcfstats \
#               -p $(pwd)/${dir} \
#               --main-title "Unfiltered Variants" \
#               ${vcf}.vchk \
#               2>> ../"logfile${logNum}_${stepName}"STDERR
        testExit "$?" "${stepName}"
#               mv ${vcf}.qstats ./${dir}
#               mv ${vcf}.vchk ./${dir}

        cd ..

        printf "${stepNAME}\n" >> checkpoint_${plate}
else
        printf "\t${stepNAME} is in the checkpoint file. This step will be passed\n" | tee -a "${logfile}"
fi

echo ========================================================================

stepName="variantVcfToolsFilter"
# Intent: Hard filter variants to keep only high quality  bi-allelic SNP, retain a subset.

((logNum++))
stepNAME=$(echo ${stepName^^})
stepGet=$(grep "${stepNAME}" checkpoint_${plate})
if [ "${stepGet}" != "${stepNAME}" ]
        then
        printf "\nBeginning step: ${stepName}\n" | tee -a "${logfile}"

        cd data

        #...options https://vcftools.github.io/index.html
	#OPTION
	#--remove-filtered-all remove sites with a filter tag
	#--mac <INT> Include only sites with Minor Allele Count greater than or equal to INT.Allele count is simply the number of times that allele appears over all individuals at that site
	#--min-alleles <INT>
	#--max-alleles <integer> Include only sites with a number of alleles greater than or equal to INT and less than or equal to integer. One of these options may be used without the other. For example, to include only bi-allelic sites, one could use: --min-alleles 2 --max-alleles 2
	#--max-missing 1.0 remove sites with any missing genotypes
	#--max-missing <float> Exclude sites on the basis of the proportion of missing data (defined to be between 0 and 1, where 0 allows sites that are completely missing and 1 indicates no missing data allowed).
	#https://vcftools.github.io/man_latest.html
        vcftools \
                --vcf variantvarfilter.vcf \
                --remove-filtered-all \
                --max-missing 0.5 \
                --remove-indels \
                --mac 1 \
                --min-alleles 2 \
                --max-alleles 2 \
                --recode \
                --out variantvcftoolsfiltered \
                &> ../"logfile${logNum}_${stepName}"

	testExit "$?" "${stepName}"

	cd ..

        printf "${stepNAME}\n" >> checkpoint_${plate}
else
        printf "\t${stepNAME} is in the checkpoint file. This step will be passed\n" | tee -a "${logfile}"
fi

echo ========================================================================
# this '1' is a one to prevent patern matching
stepName="variantVcfToolsFi1terQC"

((logNum++))
stepNAME=$(echo ${stepName^^})
stepGet=$(grep "${stepNAME}" checkpoint_${plate})
if [ "${stepGet}" != "${stepNAME}" ]
        then
        printf "\nBeginning step: ${stepName}\n" | tee -a "${logfile}"

        cd data

	#specify step variables
	vcf="variantvcftoolsfiltered.recode.vcf"
        dir=$(echo $vcf | sed 's:.vcf::')

        #quality stats for human readers
        printf "Begin Qstats\n\n" > ../"logfile${logNum}_${stepName}"STDERR
        #create a header
        printf "\nNote: This command discards indels." > ${vcf}.qstats
        printf "\nOUTPUT\n" >> ${vcf}.qstats
	printf "\nQUAL\n\tnon-indel\n\t\tSNPs\n\t\t\ttransitions\n\t\t\t\tjoint\n\t\t\t\t\tts/tv\n\t\t\t\t\t\tjoint/ref\n\t\t\t\t\t\t\tjoint/non-indel\n" >> ${vcf}.qstats
        vcfutils.pl qstats \
                ${vcf} \
                >> ${vcf}.qstats \
                2>> ../"logfile${logNum}_${stepName}"STDERR
        #visualize quality stats
        printf "\n\nBegin Bcftools Stats\n\n" >> ../"logfile${logNum}_${stepName}"STDERR
        bcftools stats \
                -F ../refgenome/"${refgen}" \
                ${vcf} \
                > ${vcf}.vchk \
                2>> ../"logfile${logNum}_${stepName}"STDERR
#...needs install of latex to generate pdfs
#       printf "\n\nBegin Plot-VcfStats\n\n" >> ../"logfile${logNum}_${stepName}"STDERR
#       plot-vcfstats \
#               -p $(pwd)/${dir} \
#               --main-title "Unfiltered Variants" \
#               ${vcf}.vchk \
#               2>> ../"logfile${logNum}_${stepName}"STDERR
        testExit "$?" "${stepName}"
#               mv ${vcf}.qstats ./${dir}
#               mv ${vcf}.vchk ./${dir}
#		mv ${refStats} ./${refStatsDir}

        cd ..

        printf "${stepNAME}\n" >> checkpoint_${plate}
else
        printf "\t${stepNAME} is in the checkpoint file. This step will be passed\n" | tee -a "${logfile}"
fi

echo ========================================================================

stepName="impute"

((logNum++))
stepNAME=$(echo ${stepName^^})
stepGet=$(grep "${stepNAME}" checkpoint_${plate})
if [ "${stepGet}" != "${stepNAME}" ]
        then
        printf "\nBeginning step: ${stepName}\n" | tee -a "${logfile}"

        cd data

	#OPTION
	#...
	#https://www.g3journal.org/content/10/1/177
	#https://www.ncbi.nlm.nih.gov/pmc/articles/PMC6322752/
	beagle \
		gt=variantvcftoolsfiltered.recode.vcf \
		nthreads="${nbcor}" \
		out=SNP_imputed \
		&> ../"logfile${logNum}_${stepName}"

	testExit "$?" "${stepName}"

	#unzip
	gzip -d SNP_imputed.vcf.gz
	#move log
	mv SNP_imputed.log "logfile${logNum}_${stepName}"
	mv "logfile${logNum}_${stepName}" ../

	cd ..

        printf "${stepNAME}\n" >> checkpoint_${plate}
else
        printf "\t${stepNAME} is in the checkpoint file. This step will be passed\n" | tee -a "${logfile}"
fi

echo ========================================================================

stepName="exportSpreadsheet"

((logNum++))
stepNAME=$(echo ${stepName^^})
stepGet=$(grep "${stepNAME}" checkpoint_${plate})
if [ "${stepGet}" != "${stepNAME}" ]
        then
        printf "\nBeginning step: ${stepName}\n" | tee -a "${logfile}"

        cd data

        grep "#CHROM" SNP_imputed.vcf > vcf_header
        python ../vcf2txt.py "SNP_imputed.vcf"

	testExit "$?" "${stepName}"

		rm vcf_header

        cd ..

        printf "${stepNAME}\n" >> checkpoint_${plate}
else
        printf "\t${stepNAME} is in the checkpoint file. This step will be passed\n" | tee -a "${logfile}"
fi


echo ========================================================================

stepName="cleanUp"

((logNum++))
stepNAME=$(echo ${stepName^^})
stepGet=$(grep "${stepNAME}" checkpoint_${plate})
if [ "${stepGet}" != "${stepNAME}" ]
        then
        printf "\nBeginning step: ${stepName}\n" | tee -a "${logfile}"


	name="${flowcell}_${TIMESTAMP}_"

	#clean program level
        mv logfile* ./results
        cp ${1} ./results
	cat deepGBSII.sh > ./results/codeState_deepGBSII.sh

	#clean in data
	cd data
	mv variant* ../results
	mv SNP* ../results
	cd ..

	#pack results
	cd results
	#rename all files
	for i in *
		do
		mv "$i" "${name}${i}"
	done

	mkdir states
		mv "${name}${bamlist}" ./states/
		mv "${name}${1}" ./states/
		mv "${name}${flowcell}"_[12]* ./states/
		mv "${name}${flowcell}"_mergedBarcodes ./states/
		mv "${name}"codeState_deepGBSII.sh ./states/

	mkdir variants
		mv "${name}"variantcall.vcf* ./variants/
		mv "${name}"variantfilter.vcf* ./variants/
		mv "${name}"variantvarfilter.vcf* ./variants/
		mv "${name}"variantvcftoolsfiltered.recode.vcf* ./variants/

	#store logfiles
	mkdir logs
		mv "${name}"logfile* ./logs
		cd logs
		#remove empty logs
		for i in "${name}"logfile*
		        do
		        if [ "$(cat ${i})" == "" ]
		                then
		                rm ${i}
		        fi
		done
		cd ..
	#up to program level
	cd ..

	#rename results and move to resultsStored
	name="deepGBSII_${flowcell}_${TIMESTAMP}"
	mv ./results ./${name}
	mv ./${name} ./resultsStored/
	mkdir ./results


	#export and final cleanup
	outputFN "${name}"

        printf "${stepNAME}\n" >> checkpoint_${plate}
else
        printf "\t${stepNAME} is in the checkpoint file. This step will be passed\n" | tee -a "${logfile}"
fi

echo ========================================================================
printf  "The deepGBSII paired data pipeline has completed sucessfully. \n" | tee -a "${logfile}"
echo ========================================================================
#rm checkpoint_${plate}
exit

END
SKIP
