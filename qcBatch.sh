#!/bin/bash

#This is not complete

#runs fastQC on any valid files in dataLib and then runs multiQC
#exports results to resultsStored.

function callFastQC {
	#takes a single arg, creates a file path from sym link, and calls fast QC
	echo "arg: ${1}"
	file=$(cd ./${1}; ls; cd ..)
	reads="./${1}/${file}"

	echo "path to reads: "${reads}

	fastqc \
		${reads} \
		-o "$(pwd)/fqc" \
		--extract \
}

function iterateCall {
        #takes a space delim string, sends calls to callFastQC function
        for i in "$@"
        do
                echo "calling fastQC for ${i}"
                callFastQC "$i"
        done
}


echo ==================================

cd dataLib

#ensure fqc is empty and exists
if [ -e fqc ]
	then
	rm -r fqc
	mkdir fqc
else
	mkdir fqc
fi

#make list of sym links to reads
fastaStr=""
for i in *.fq.gz
	do
	fastaStr="${fastaStr} ${i}"
done

iterateCall "${fastaStr}"

# call multiqc
cd fqc
	#call...
cd ..

# rename package and send to  resultsStored
timestamp=...ymdhm
name="QualityControl_${timestamp}
mv fqc ${name}
mv ${name} ../resultsStored
